using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Axle
{
    public GameObject left;
    public GameObject right;
}

public class LunohodController : MonoBehaviour
{
    [SerializeField] private List<Axle> axles;
    [SerializeField] private float Acceleration;
    
    public void FixedUpdate()
    {
        float vertical = Acceleration * Input.GetAxis("Vertical");
        float horizontal = Acceleration * Input.GetAxis("Horizontal");

        foreach (Axle wheels in axles)
        {
            JointMotor motor = wheels.left.GetComponent<HingeJoint>().motor;

            motor.targetVelocity = horizontal + vertical;
            wheels.left.GetComponent<HingeJoint>().motor = motor;

            motor.targetVelocity = -horizontal + vertical;
            wheels.right.GetComponent<HingeJoint>().motor = motor;
        }
    }
}